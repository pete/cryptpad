import subprocess
import logging

from os.path import getmtime

try:
    # Shibboleth
    last_shib = getmtime('/var/run/last_shib_update')
    shib_timestamp = getmtime('/etc/shibboleth/webservicesDynamic/shibboleth2.xml')

    # Vhost - The mtime on the directory changes when a file or a subdirectory is modified
    last_vhost = getmtime('/var/run/last_vhost_update')
    vhost_timestamp = getmtime('/etc/httpd/conf.d/vhost/')

    if  shib_timestamp > last_shib and vhost_timestamp > last_vhost:
        # Run configtest
        cfg_check = subprocess.call(['apachectl', 'configtest'])
        if cfg_check == 0:
            # Send graceful restart signal to apache
            subprocess.call(['kill', '-USR1', '1'])

            # Update timestamp to last_cfg_reload
            subprocess.call(['touch', '/var/run/last_shib_update'])
            subprocess.call(['touch', '/var/run/last_vhost_update'])
except Exception as e:
    logging.error("Not able to reload Apache config: FAILED" + repr(e))
    print(repr(e))

try:
    subprocess.call(['cat', '/eos/project/w/webservices/wwweos/readiness'])
except Exception as e:
    logging.error("EOS permission denied" + repr(e))
    print(repr(e))
