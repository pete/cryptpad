#!/bin/sh

# For each variable:
# -If file is provided as env variable, the config file in '/' is
# overwrittern
# - In any case, we replace the environment variables
if [[ -z "$HOSTNAME_FQDN" ]]; then
  export HOSTNAME_FQDN="${NAMESPACE}.web.cern.ch"
fi

# Replace hostname in shibboleth2.xml template
#envsubst < /shibboleth2.xml > /etc/shibboleth/shibboleth2.xml

# If there are files in /tmp/configmap that are not empty
# (overriden by a ConfigMap) copy them to /etc/shibboleth
#if [ -n "$(ls -A /tmp/configmap)" ]
#then
#  for f in /tmp/configmap/*
#  do
#    if [ -s $f ]
#    then
#      cp /tmp/configmap/* /etc/shibboleth/
#    fi
#  done
#fi

if [[ -z $SERVICE_HOST || -z $SERVICE_PORT ]]; then
  # Transform hyphen into underscore as expected
  FORMATTED_NAME=`echo $SERVICE_NAME | tr '-' '_'`
  # Dynamically obtain the service with bash magic
  export SERVICE_HOST=$(eval echo "\${${FORMATTED_NAME^^}_SERVICE_HOST}")
  export SERVICE_PORT=$(eval echo "\${${FORMATTED_NAME^^}_SERVICE_PORT}")
fi

exec httpd -DFOREGROUND
