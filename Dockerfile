FROM node:latest
MAINTAINER Peter Jones <peter.l.jones@cern.ch>
LABEL description="Default Cryptpad based on Arno"

ENV SOURCE_URL="https://github.com/xwiki-labs/cryptpad"

# Install Cryptpad from the GitHub repo, master branch
RUN git clone ${SOURCE_URL} \
   && cd cryptpad \
   && npm install \
   && npm install -g bower \
   && bower install --allow-root

# Copies the config.js, with logging to stdout set to true
RUN chmod -R 777 /cryptpad

COPY config.js /cryptpad/

WORKDIR /cryptpad

EXPOSE 3000

ENTRYPOINT ["node"]
CMD ["./server.js"]
